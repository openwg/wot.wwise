﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using xliff_translator.Extensions;

namespace xliff_translator
{
    class Converter
    {
        string[] extensions = { "wwu" };
        string directory = "";

        XLIFF.Document xlfdoc;

        public Converter(string directory)
        {
            this.directory = directory;
            xlfdoc = new XLIFF.Document();
            
        }

        public void Export(string xliffFile)
        {
            foreach (var extension in extensions)
            {
                var files = Directory.EnumerateFiles(directory, $"*.{extension}", SearchOption.AllDirectories);
                foreach (var filepath in files)
                {
                    var xlf_file = new XLIFF.XLFFile(directory,Helper.MakeRelativePath(directory,filepath));
                    xlfdoc.AddFile(xlf_file);
                }
            }
            xlfdoc.SaveXLIFF(xliffFile);
        }

        public void Import(string xliffFile)
        {
            xlfdoc.OpenXLIFF(xliffFile);
            foreach (var extension in extensions)
            {
                var files = Directory.EnumerateFiles(directory, $"*.{extension}", SearchOption.AllDirectories);
                foreach (var filepath in files)
                {
                    xlfdoc.PatchWWise(directory, Helper.MakeRelativePath(directory, filepath));
                }
            }
        }

    }
}
