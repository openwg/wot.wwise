﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Linq;

namespace xliff_translator.Extensions
{
    public static class XElementExtensions
    {
        public static string GetPath(this XElement node)
        {
            string path = node.Name.ToString();
            XElement currentNode = node;
            while (currentNode.Parent != null)
            {
                currentNode = currentNode.Parent;
                path = currentNode.Name.ToString() + @"\" + path;
            }
            return path;
        }
    }
}
