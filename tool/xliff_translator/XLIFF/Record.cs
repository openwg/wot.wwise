﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Linq;

namespace xliff_translator.XLIFF
{
    class Record
    {
        public string id = "";
        public string source = "";
        public string target = "";

        private string[] blackList = { "Impulse response created by", "Tip", "Good" };

        public Record()
        {
            
        }

        public static Record FromWWiseNode(XElement commentNode)
        {
            var record = new Record();
            record.ImportCommentNode(commentNode);
            return record;
        }

        public static Record FromXLIFFNode(XElement xliffNode)
        {
            var record = new Record();
            record.ImportXLIFFNode(xliffNode);
            return record;
        }

        public void ImportCommentNode(XElement node)
        {
            source = node.Value.Trim('\n').Trim().Replace("\n", @"\n");

            foreach(var blEntry in blackList)
            {
                if (source.StartsWith(blEntry))
                {
                    source = "";
                }
            }

            id = node.Parent.Attribute("Name").Value;
        }

        public void ImportXLIFFNode(XElement node)
        {
            id = node.Attribute("id").Value;
            source = node.Element("source").Value;
            target = node.Element("target").Value;
        }

        public XElement ExportXLIFF()
        {
            if(source=="")
            {
                return null;
            }

            var tu = new XElement("trans-unit");
            tu.Add(new XAttribute("id",id));
            tu.Add(new XAttribute("resname", id));

            var xsource = new XElement("source");
            xsource.Value = source;
            tu.Add(xsource);

            var xtarget = new XElement("target");
            xtarget.Value = target;
            tu.Add(xtarget);

            return tu;
        }
    }
}
