﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml.Linq;

namespace xliff_translator.XLIFF
{
    class XLFFile
    {
        public List<Record> records;
        public string relativePath;

        public XLFFile(string pathRoot, string relativePath)
        {
            this.relativePath = relativePath;
            records = new List<Record>();
            ImportWWise(Path.Combine(pathRoot,relativePath));
        }

        public XLFFile(XElement xliffNode)
        {
            records = new List<Record>();
            ImportXLIFF(xliffNode);
        }

        public void ImportWWise(string WWiseXMLPath)
        {
            var xlfdoc = XDocument.Load(WWiseXMLPath);
            foreach(var element in xlfdoc.Descendants("Comment"))
            {
                records.Add(Record.FromWWiseNode(element));
            }
        }

        public void ImportXLIFF(XElement xliffNode)
        {
            relativePath = xliffNode.Attribute("original").Value;
            foreach(var tu in xliffNode.Descendants("trans-unit"))
            {
                records.Add(Record.FromXLIFFNode(tu));
            }
        }

        public XElement ExportXLIFF()
        {
            if(records.Where(x=>x.source!="").Count()==0)
            {
                return null;
            }

            var file = new XElement("file");
            file.Add(new XAttribute("original", relativePath));
            file.Add(new XAttribute("source-language", "ru"));
            file.Add(new XAttribute("target-language", "en"));
            file.Add(new XAttribute("datetype", "plaintext"));

            var body = new XElement("body");
            file.Add(body);

            foreach(var record in records)
            {
                body.Add(record.ExportXLIFF());
            }

            return file;
        }

        public void PatchWWise(string filepath)
        {
            var wwise_doc = XDocument.Load(filepath);

            foreach(var wwise_comment in wwise_doc.Descendants("Comment"))
            {
                var comment_id = wwise_comment.Parent.Attribute("Name").Value;

                var xlf_record = records.Where(x => x.id == comment_id).FirstOrDefault();

                if(xlf_record!=null)
                {
                    if(xlf_record.target!="")
                    {
                        Console.WriteLine($"Replacing comment {relativePath}:{comment_id}");
                        wwise_comment.Value = xlf_record.target.Replace(@"\n","\n");
                    }
                }

            }

            File.WriteAllText(filepath, wwise_doc.ToString(SaveOptions.None));
        }
    }
}
