﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Linq;
using System.IO;
using System.Linq;

namespace xliff_translator.XLIFF
{
    class Document
    {
        List<XLFFile> files;

        public Document()
        {
            files = new List<XLFFile>();
        }

        public void AddFile(XLFFile file)
        {
            files.Add(file);
        }


        public void OpenXLIFF(string filepath)
        {
            var xdoc = XDocument.Load(filepath);
            foreach(var file in xdoc.Descendants("file"))
            {
                files.Add(new XLFFile(file));
            }
        }

        public void SaveXLIFF(string filepath)
        {
            var xdoc = new XDocument();

            var xliff = new XElement("xliff");
            xdoc.Add(xliff);

            xliff.Add(new XAttribute("version", "1.2"));

            foreach(var file in files.Where(x=>x.records.Count>0))
            {
                xliff.Add(file.ExportXLIFF());
            }

            File.WriteAllText(filepath, xdoc.ToString(SaveOptions.None));
        }

        public void PatchWWise(string directory, string relativePath)
        {
            var file = files.Where(x => x.relativePath == relativePath).FirstOrDefault();
            if(file==null)
            {
                return;
            }
            file.PatchWWise(Path.Combine(directory, relativePath));
        }
    }
}
