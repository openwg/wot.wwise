﻿using System;
using System.IO;

namespace xliff_translator
{
    class Program
    {


        static void MakeNewLanguage(string sourceLanguage, string targetLanguage)
        {
            Helpers.DirectoryHelper.DirectoryCopy($"../../wwise_project/{sourceLanguage}/", $"../../wwise_project/{targetLanguage}/", true);
            var converter = new Converter($"../../wwise_project/{targetLanguage}/");
            converter.Import($"../../xliff/{targetLanguage}.xlf");
        }

        static void ExportLanguage(string lang)
        {
            var converter = new Converter($"../../wwise_project/{lang}/");
            converter.Export($"../../xliff/{lang}.xlf");
        }

        static void Main(string[] args)
        {
            Directory.SetCurrentDirectory(Path.Combine(Directory.GetCurrentDirectory(),"../../../"));
            Console.OutputEncoding = System.Text.Encoding.Unicode;

            ExportLanguage("ru");
            MakeNewLanguage("ru", "en");


            Console.WriteLine("Press any key...");
            Console.ReadKey();
        }
    }
}